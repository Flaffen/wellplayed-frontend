import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './Dashboard.css';

import Header from '../Header';
import Main from '../Main';
import Playgrounds from '../Playgrounds';

class Dashboard extends Component {
  render() {
    return (
      <div>
        <Header user={this.props.user} />
        <Route exact path="/" component={() => {
          return (
            <div>
              <Main user={this.props.user} />
            </div>
          );
        }} />
        <Route path="/playgrounds" component={(match) => { return <Playgrounds match={match.match} user={this.props.user} /> }} />
      </div>
    );
  }
}

export default Dashboard;
