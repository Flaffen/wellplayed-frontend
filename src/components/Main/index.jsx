import React, { Component } from 'react';

import Navbar from '../Navbar';

class Main extends Component {
  componentDidMount() {
    document.body.classList.remove('body-landing');
    document.body.classList.add('body-bashboard');
  }

  render() {
    return (
      <main className="main-info">
        <Navbar />
        <div className="user-background-field">
          <div className="user-background-field-wallpaper" />
          <div className="user-background-field-navigation">
            <div className="user-big-field-photo">
              <img className=" user-big-field-photo__pic" src={this.props.user.img} alt="" />
            </div>
            <span className="user-background-field-navigation__user-name">{this.props.user.first_name + ' ' + this.props.user.last_name}</span>
            <ul className="user-background-field-navigation-bar">
              <li className="user-background-field-navigation__field_isActive">Стена <span /></li>
              <li className="user-background-field-navigation__field">Активность<span /></li>
              <li className="user-background-field-navigation__field">Друзья<span /></li>
              <li className="user-background-field-navigation__field">Площадки<span /></li>
              <li className="user-background-field-navigation__field">Инвентарь<span /></li>
            </ul>
          </div>
        </div>
        <div className="favorites-playgrounds">
          <span>Избранные площадки</span>
          {/*
          <div className="favorites-playgrounds-bar">
            <img className="favorites-playground__pic" src="/media/pictures/pg-pics/pg.jpeg" alt="" />
            <span className="favorites-playground__name">БК "Волжанин"</span>
          </div>
          <div className="favorites-playgrounds-bar">
            <img className="favorites-playground__pic" src="/media/pictures/pg-pics/pg.jpeg" alt="" />
            <span className="favorites-playground__name">"Йога шала"</span>
          </div>
          <div className="favorites-playgrounds-bar">
            <img className="favorites-playground__pic" src="/media/pictures/pg-pics/pg.jpeg" alt="" />
            <span className="favorites-playground__name">"Пляжка"</span>
          </div>
          <div className="favorites-playgrounds-bar">
            <img className="favorites-playground__pic" src="/media/pictures/pg-pics/pg.jpeg" alt="" />
            <span className="favorites-playground__name">"Олимп"</span>
          </div>
          <div className="favorites-playgrounds-bar">
            <img className="favorites-playground__pic" src="/media/pictures/pg-pics/pg.jpeg" alt="" />
            <span className="favorites-playground__name">"18 микрорайон"</span>
          </div>
          */}
        </div>
      </main>
    );
  }
}

export default Main;
