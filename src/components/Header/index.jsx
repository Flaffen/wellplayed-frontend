import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {
  render() {
    return (
      <header className="main-header">
        <div className="logo">
          <a style={{display: 'inline-block', height: '100%', width: '100%'}}>
            <img alt="" className="logo__pic" src="/media/pictures/header-pic/logo-without-bg.png" height="44px" />
          </a>
        </div>
        <div className="user-small-field">
          <div className="user-small-field-photo">
            <img alt="" src={this.props.user.img}/>
          </div>
          <span className="user-small-field__name">{this.props.user.first_name + ' ' + this.props.user.last_name}</span>
        </div>
        <div className="scores-block" />
        <div className="manage-profile-button">
          <svg width="12px" height="13px" viewBox="0 0 12 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <defs />
            <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd" strokeLinecap="square">
              <g id="Desktop-HD" transform="translate(-45.000000, -157.000000)" stroke="#FFFFFF">
                <g id="left-navigation-panel">
                  <g id="Group" transform="translate(36.000000, 150.000000)">
                    <g id="Group-8" transform="translate(9.000000, 7.000000)">
                      <path d="M0.5,2.5 L4.5,2.5" id="Line-8" />
                      <path d="M2.5,9 L2.5,12" id="Line-12" strokeWidth={2} />
                      <path d="M10.5,2.5 L10.5,12" id="Line-12" strokeWidth={2} />
                      <path d="M2.5,0.5 L2.5,4.5" id="Line" />
                      <path d="M6.5,5.5 L6.5,12" id="Line-12" strokeWidth={2} />
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </svg>
          <span>заполнить профиль</span>
        </div>
        <nav className="main-navigation">
          <ul className="main-navigation-fields">
            <Link to="/">
              <li className="main-navigation-field">
                <span className="main-navigation-field__text">Моя страница</span>
              </li>
            </Link>
            <Link to="/playgrounds">
              <li className="main-navigation-field">
                <span className="main-navigation-field__text">Площадки</span>
              </li>
            </Link>
            <Link to="/calendar">
              <li className="main-navigation-field">
                <span className="main-navigation-field__text">Календарь</span>
              </li>
            </Link>
            <Link to="/market">
              <li className="main-navigation-field">
                <span className="main-navigation-field__text">Маркет</span>
              </li>
            </Link>
            <Link to="/coach">
              <li className="main-navigation-field">
                <span className="main-navigation-field__text">Тренераская</span>
              </li>
            </Link>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
