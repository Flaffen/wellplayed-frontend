import React, { Component } from 'react';

class Page3 extends Component {
  render() {
    return (
      <div>
        <div className="landing-slogan">
          <p><span>Твои усилия заслуживают вознаграждения<span style={{color: 'red'}}>.</span></span></p>
        </div>
        <div className="landing-description">
          <span>Быть постоянно активным и мотивированным не всегда просто<br />мы решили этот вопрос раз и навсегда,<br />выдавая пользователям различные активные задания,
            <br />например регулярное посещение тренажерного зала,<br /> игр в баскетбол или йога-залов, вознаграждается теперь в двойне.</span>
          <br />
          <br />
        </div>
      </div>
    );
  }
}

export default Page3;
