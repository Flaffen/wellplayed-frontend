import React, { Component } from 'react';
import axios from 'axios';

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: ''
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleLoginChange(event) {
    this.setState({
      login: event.target.value
    });
  }

  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.get(`/api/v1/auth/signin?email=${this.state.login}&password=${this.state.password}`)
    .then(res => {
      if (res.data.success) {
        this.props.handleLogin(res.data.token);
      }
    });
  }

  render() {
    return (
      <div className="login-form col-lg-3">
        <form onSubmit={this.handleSubmit} className="reg-form-fields" action method="post">
          <label htmlFor="user-nick-name">Логин:</label>
          <input value={this.state.login} onChange={this.handleLoginChange} className="reg-form-item" type="text" id="user-nick-name" placeholder="Введите логин" /><br />
          <label htmlFor="user-pass">Пароль:</label>
          <input value={this.state.password} onChange={this.handlePasswordChange} className="reg-form-item " type="password" id="user-pass" placeholder="Введите пароль" /><br />
          <input className="reg-form-item-btn" type="submit" id="send" name="send" value="Войти" defaultValue="Вход" />
        </form>
      </div>
    );
  }
}

export default SignIn;
