import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Landing.css';

import Page1 from './Page1';
import Page2 from './Page2';
import Page3 from './Page3';
import Page4 from './Page4';
import SignIn from './SignIn';

class Landing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1
    };

    this.changePage = this.changePage.bind(this);
    this.handleLogin = this.handleLogin.bind(this);

    document.body.classList.add('body-landing');
    document.body.classList.remove('body-bashboard');
  }

  changePage(pageNum) {
    this.setState({
      page: pageNum
    });
  }

  handleLogin(token) {
    this.props.handleLogin(token);
  }

  render() {
    var CurrentPage;

    switch (this.state.page) {
      case 1:
        CurrentPage = Page1;
        break;
      case 2:
        CurrentPage = Page2;
        break;
      case 3:
        CurrentPage = Page3;
        break;
      case 4:
        CurrentPage = Page4;
        break;
      case 5:
        CurrentPage = () => { return <SignIn handleLogin={this.handleLogin} />; };
        break;
      default:
        CurrentPage = Page1;
    }

    return (
      <div>
        <nav className="landing-left-navigation">
          <div className="landing-left-navigation-indication-bar">
            <div onClick={() => { this.changePage(1); }} className={this.state.page === 1 ? 'dot-border_redColor' : 'dot-border'}>
              <span className={this.state.page === 1 ? 'dot_redColor' : 'dot'} />
            </div>
            <div onClick={() => { this.changePage(2); }} className={this.state.page === 2 ? 'dot-border_redColor' : 'dot-border'}>
              <span className={this.state.page === 2 ? 'dot_redColor' : 'dot'} />
            </div>
            <div onClick={() => { this.changePage(3); }} className={this.state.page === 3 ? 'dot-border_redColor' : 'dot-border'}>
              <span className={this.state.page === 3 ? 'dot_redColor' : 'dot'} />
            </div>
            <div onClick={() => { this.changePage(4); }} className={this.state.page === 4 ? 'dot-border_redColor' : 'dot-border'}>
              <span className={this.state.page === 4 ? 'dot_redColor' : 'dot'} />
            </div>
          </div>
          <div className="landing-left-navigation-digital-bar">
            <span className="landing-left-navigation-digital-bar__underline" />
            <span className="landing-left-navigation-digital-bar__digits">00</span>
          </div>
          <span className="landing-left-navigation__sign">well played.</span>
        </nav>
        <header className="landing-header">
          <nav className="landing-header-nav-menu">
            <ul className="landing-header-nav-menu-list">
              {/*<li class="landing-header-nav-menu-list__href"><a href="">Поиск площадок</a></li>
        <li class="landing-header-nav-menu-list__href"><a href="">Маркетплейс</a></li>
        <li class="landing-header-nav-menu-list__href"><a href="">Отслеживание активности</a></li>
        <li class="landing-header-nav-menu-list__href"><a href="">О нас</a></li>*/}
            </ul>
          </nav>
        </header>
        <div className="landing-right-block">
          <a onClick={() => { this.changePage(5); }}>Вход</a>
          <Link to="/signup">Регистрация</Link>
        </div>
        <main>
          <CurrentPage />
        </main>
        <footer className="landing-footer">
          <a className="landing-footer-social" href><i className="fab fa-angellist" /></a>
          <a className="landing-footer-social" href><i className="fab fa-facebook-f" /></a>
          <a className="landing-footer-social" href><i className="fab fa-instagram" /></a>
          {/*<span>facebook - YouTube - VK - instagram</span>*/}
        </footer>
      </div>
    );
  }
}

export default Landing;
