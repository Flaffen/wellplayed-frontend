import React, { Component } from 'react';

class Page2 extends Component {
  render() {
    return (
      <div>
        <div className="landing-slogan">
          <p><span>Проще быть активным вместе с well played<span style={{color: 'red'}}>.</span></span></p>
        </div>
        <div className="landing-description">
          <span>Учавствуй и отслеживай свою персональную или групповую активность<br />найди и пообщайся с персональными тренерами,<br />трать свое вознаграждение в виде баллов в нашем маркетплейсе
            <br />вызывай на соревнования своих друзей,<br /> well played создан для объединения людей ведущих спортивный образ жизни</span>
          <br />
          <br />
        </div>
      </div>
    );
  }
}

export default Page2;
