import React, { Component } from 'react';
import cookie from 'react-cookies';

class Navbar extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    cookie.remove('token');
    window.location = '/';
  }

  render() {
    return (
      <nav className="upper-navigation-bar">
        <div className="logout-button">
          <i onClick={this.handleLogout} className="fa fa-power-off" />
        </div>
      </nav>
    );
  }
}

export default Navbar;
