import React, { Component } from 'react';
import axios from 'axios';

import Navbar from '../../Navbar';
import Address from './Address';
import Going from './Going';
import Chat from './Chat';

class Playground extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playground: {
        going: [],
        members: []
      },
      loading: true
    };

    this.updateGoing = this.updateGoing.bind(this);
  }

  componentDidMount() {
    // axios.defaults.headers.common['x-access-token'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmE3N2Q5ZTE2YTkyYzA3OTI3MzMzMWMiLCJpYXQiOjE1Mzc3MDM1NzJ9.rk_IAvjPqRbruoclnFWhYCekj6S1uXzM4EOMw7ZA_tk';
    axios.get('/api/v1/playgrounds/' + this.props.match.params.playgroundId)
      .then(res => {
        this.setState({
          playground: res.data.playground,
          loading: false
        });
      });
  }

  updateGoing(going) {
    this.setState({
      playground: {
        ...this.state.playground,
        going: going
      }
    });
  }

  render() {
    if (!this.state.loading) {
      return (
        <main className="main-info">
          <Navbar />
          <Address playground={this.state.playground} user={this.props.user} />
          <Going playground={this.state.playground} user={this.props.user} updateGoing={this.updateGoing} />
          <Chat playground={this.state.playground} />
          <div className="favorites-playgrounds">
            <span>Информация</span>
          </div>
        </main>
      );
    } else {
      return <p>Loading...</p>;
    }
  }
}

export default Playground;
