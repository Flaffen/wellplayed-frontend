import React, { Component } from 'react';
import { withGoogleMap, withScriptJs, GoogleMap, Marker } from 'react-google-maps';
import axios from 'axios';

class Address extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMember: false
    };

    this.handleEnter = this.handleEnter.bind(this);
    this.handleQuit = this.handleQuit.bind(this);
  }

  componentDidMount() {
    if (this.props.playground.members.indexOf(this.props.user._id) !== -1) {
      this.setState({
        isMember: true
      });
    }
  }

  handleEnter() {
    axios.post('/api/v1/playgrounds/' + this.props.playground._id)
      .then(res => {
        console.log(res);

        if (res.data.success) {
          this.setState({
            isMember: true
          });
        }
      });
  }

  handleQuit() {
    axios.post('/api/v1/playgrounds/' + this.props.playground._id + '/cancel')
      .then(res => {
        console.log(res);

        if (res.data.success) {
          this.setState({
            isMember: false
          });
        }
      });
  }

  render() {
    const GoogleMapExample = withGoogleMap((props) => {
      return (
        <GoogleMap
          defaultCenter={{ lat: this.props.playground.lat, lng: this.props.playground.lng }}
          defaultZoom={8}
        >
        <Marker position={{ lat: this.props.playground.lat, lng: this.props.playground.lng }}/>
        </GoogleMap>
      );
    });

    return (
      <div className="playground-map-field">
        <div className="playground-map-wallpaper">
          {/* <iframe title="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2628.854404599166!2d44.7482348156718!3d48.78466977928029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41054c9232ad3e8b%3A0xf07d8df2bd2b6721!2z0JHQsNGB0LrQtdGC0LHQvtC70YzQvdGL0Lkg0JrQu9GD0LEgItCS0L7Qu9C20LDQvdC40L0i!5e0!3m2!1sru!2sru!4v1538215996276" width="100%" height="100%" frameBorder={0} style={{border: 0}} allowFullScreen /> */}
          <GoogleMapExample
            containerElement={<div style={{ height: '100%', width: '100%' }}></div>}
            mapElement={<div style={{ height: '100%' }}></div>}
          />
        </div>
        <div className="playground-name-and-address">
          <span className="playground-map-field__playground-name">{this.props.playground.name}: </span>
          <span className="playground-map-field__playground-address">{this.props.playground.address}</span>
        </div>
        <div className="playground-map-field-signIn-button">
          {this.state.isMember ?
            <span onClick={this.handleQuit} className="playground-map-field-signIn-button__text">
              Выйти
            </span>
          :
            <span onClick={this.handleEnter} className="playground-map-field-signIn-button__text">
              Вступить
            </span>
          }
        </div>
      </div>
    );
  }
}

export default Address;
