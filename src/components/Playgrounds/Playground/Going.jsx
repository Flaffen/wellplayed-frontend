import React, { Component } from 'react';
import axios from 'axios';

class Going extends Component {
  constructor(props) {
    super(props);

    this.state = {
      going: false
    };

    this.handleGoing = this.handleGoing.bind(this);
    this.handleGoingQuit = this.handleGoingQuit.bind(this);
  }

  componentDidMount() {
    if (this.props.playground.going.indexOf(this.props.user._id) !== -1) {
      this.setState({
        going: true
      });
    }
  }

  handleGoing() {
    axios.post('/api/v1/playgrounds/' + this.props.playground._id + '/going')
      .then(res => {
        if (res.data.success) {
          this.props.updateGoing(res.data.playground.going);
          this.setState({
            going: true
          });
        }
      });
  }

  handleGoingQuit() {
    axios.post('/playgrounds/' + this.props.playground._id + '/going/cancel')
      .then(res => {
        if (res.data.success) {
          this.props.updateGoing(res.data.playground.going);
          this.setState({
            going: false
          });
        }
      });
  }

  render() {
    return (
      <div className="playground-who-will-come">
        <div className="playground-who-will-come-field">
          <span className="playground-who-will-come-field__text">Сегодня придут:</span>
          <div className="playground-visitors">
            {this.props.playground.going.map((user) => {
              return (
                <div className="who-will-come-user-small-field-photo" key={user}>
                  <img alt="" src={user.img} />
                </div>
              );
            })}
          </div>
          {!this.state.going ?
            <div onClick={this.handleGoing} style={{ cursor: 'pointer' }} className="playground-i-will-come-button">
              <span className="playground-i-will-come-button__text">
                Я пойду
              </span>
            </div>
            :
            <div onClick={this.handleGoingQuit} style={{ cursor: 'pointer' }} className="playground-i-will-come-button">
              <span className="playground-i-will-come-button__text">
                Отмена
              </span>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default Going;
