import React, { Component } from 'react';
import socketIOClient from 'socket.io-client';
import Moment from 'react-moment';
import cookie from 'react-cookies';

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      newMessageContent: '',
      socket: null
    };

    this.handleMessageSend = this.handleMessageSend.bind(this);
    this.handelNewMessageContentChange = this.handelNewMessageContentChange.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }

  handleMessageSend(event) {
    event.preventDefault();

    this.state.socket.emit('chat message', this.state.newMessageContent);

    this.setState({
      newMessageContent: ''
    });
  }

  handelNewMessageContentChange(event) {
    this.setState({
      newMessageContent: event.target.value
    });
  }

  scrollToBottom() {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  }

  componentDidMount() {
    const socket = socketIOClient();

    socket.emit('authentication', cookie.load('token'));
    socket.on('authenticated', () => {
      socket.emit('room', this.props.playground._id);
      socket.emit('history');

      socket.on('history', (messages) => {
        this.setState({
          messages: messages
        });
      });

      socket.on('chat message', (message) => {
        this.setState({
          messages: this.state.messages.concat(message)
        });
      });
    });

    this.setState({
      socket: socket
    });
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    console.log(this.state.messages);

    return (
      <div className="playground-chat">
        <div className="playground-chat-upper-part">
          <span className="playground-chat-upper-part__usersInThisChannel">Участники {this.props.playground.members.length}</span>
          <span className="playground-chat-upper-part__chanelName">#Basketaball</span>
        </div>
        <div className="playground-chat-messages-field">
          {this.state.messages.map((message) => {
            return (
              <div className="playground-chat-message-fields" key={message._id}>
                <div className="playground-chat-message-fields-user-info">
                  <div className="who-will-come-user-small-field-photo">
                    <img alt="" src={message.sender.img} />
                  </div>
                  <span className="playground-chat-message-fields-user-info__name">{message.sender.first_name + ' ' + message.sender.last_name} </span>
                  <span className="playground-chat-message-fields-user-info__timeStamp"><Moment format="llll">{message.sent}</Moment></span>
                </div>
                <span className="playground-chat-message-fields-user-info__message">{message.content}</span>
              </div>
            );
          })}
          <div style={{ float: 'left', clear: 'both' }} ref={(el) => { this.messagesEnd = el; }}></div>
        </div>
        <div className="playground-chat-input-part">
          <div className="playground-chat-input-part-input-field">
            <form onSubmit={this.handleMessageSend}>
              <input type="text" placeholder="Введите текст сообщения...." value={this.state.newMessageContent} onChange={this.handelNewMessageContentChange} />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Chat;
