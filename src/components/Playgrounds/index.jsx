import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import axios from 'axios';
import { withGoogleMap, withScriptJs, GoogleMap, Marker } from 'react-google-maps';

import Playground from './Playground';

class Playgrounds extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playgrounds: [],
      selected: {}
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  componentDidMount() {
    // axios.defaults.headers.common['x-access-token'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmE3N2Q5ZTE2YTkyYzA3OTI3MzMzMWMiLCJpYXQiOjE1Mzc3MDM1NzJ9.rk_IAvjPqRbruoclnFWhYCekj6S1uXzM4EOMw7ZA_tk';
    axios.get('/api/v1/playgrounds')
      .then(res => {
        this.setState({
          playgrounds: res.data.playgrounds,
          selected: res.data.playgrounds[0]
        });
      });
  }

  handleSelect(playground) {
    this.setState({
      selected: playground
    });
  }

  render() {
    console.log(this.state);

    const GoogleMapExample = withGoogleMap(props => (
      <GoogleMap
        defaultCenter={{ lat: this.state.selected.lat, lng: this.state.selected.lng }}
        defaultZoom={8}
      >
      {this.state.playgrounds.map(playground => <Marker onClick={() => { this.handleSelect(playground); }} position={{ lat: playground.lat, lng: playground.lng }}/>)}
      </GoogleMap>
    ));

    return (
      <div>
      <Route exact path={this.props.match.url} component={() => {
      if (this.state.playgrounds) {
      return (
        <main className="main-info">
          <nav className="upper-navigation-bar">
            <div className="logout-button">
              <i className="fas fa-power-off" />
            </div>
          </nav>
        <div className="playground-search-map-field">
          <div className="playground-search-map-field__map">
            <GoogleMapExample
              containerElement={<div style={{ height: '100%', width: '100%' }}></div>}
              mapElement={<div style={{ height: '100%' }}></div>}
            />
            {/*<iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d42045.832047254626!2d44.74242045544881!3d48.80356461883581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x411ab2bf22a2389b%3A0xe66380a6f53c1448!2z0JLQvtC70LbRgdC60LjQuSwg0JLQvtC70LPQvtCz0YDQsNC00YHQutCw0Y8g0L7QsdC7Lg!5e0!3m2!1sru!2sru!4v1538309772998"
              width="100%"
              height="100%"
              frameBorder={0}
              style={{ border: 0 }}
              allowFullScreen
            />{" "}
            */}
          </div>
      <Link to={this.props.match.url + '/' + this.state.selected._id}>
      <div className="playground-search-map-field-info">
        <div className="playground-search-map-field-info-address">
          <span className="playground-search-map-field-info-address__address">
            {this.state.selected.address}
          </span>
          <span className="playground-search-map-field-info-address__name">
            {this.state.selected.name}
          </span>
          <br />
          <span className="playground-search-map-field-info-address__status_isOpen">
            Сейчас открыто
          </span>
        </div>
        <div className="playground-search-map-field-info-photo">
          <img
            className="playground-search-map-field-info-photo__pic"
            src={this.state.selected.img}
            alt=""
          />
        </div>
      </div>
      </Link>
    </div>
    <div className="playgrounds-list">
      <span>Список площадок и залов</span>
      <div className="playgrounds-list-container">
        {this.state.playgrounds.map((playground) => {
          return (
            <div
              onClick={() => { this.handleSelect(playground); }}
              className="favorites-playgrounds-bar"
              key={playground._id}
              style={{ cursor: 'pointer' }}>
              <img
                className="favorites-playground__pic"
                src={playground.img}
                alt=""
              />
              <span className="favorites-playground__name">
                {playground.name}
              </span>
            </div>            
          );
        })}
      </div>
    </div>
    <div className="playground-listed-info">
      <span>Информация</span>
    </div>
        </main>
      );
    } else {
      return <p>Loading...</p>;
    }
    }} />
    <Route
      path={this.props.match.url + '/:playgroundId'}
      component={(match) => { return <Playground match={match.match} user={this.props.user} />; }}
    />
    </div>);
  }
}

export default Playgrounds;
