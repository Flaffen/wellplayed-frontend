import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import cookie from 'react-cookies';
import axios from 'axios';

import Header from './components/Header';
import Main from './components/Main';
import Playgrounds from './components/Playgrounds';
import Landing from './components/Landing';
import Dashboard from './components/Dashboard';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
      user: {}
    };

    this.handleLogin = this.handleLogin.bind(this);
  }

  componentDidMount() {
    if (cookie.load('token')) {
      axios.get('/api/v1/auth/' + cookie.load('token'))
      .then(res => {
        console.log(res);
        if (res.data.success) {
          axios.defaults.headers.common['x-access-token'] = cookie.load('token');
          this.setState({
            loggedIn: true,
            user: res.data.user
          });
        }
      });
    }
  }

  handleLogin(token) {
    cookie.save('token', token);
    window.location = '/';
  }

  render() {
    if (!this.state.loggedIn) {
      return <Landing handleLogin={this.handleLogin} />;
    } else {
      return <Dashboard user={this.state.user} />;
    }
    // return (
    //   <div>
    //     <Route exact path="/" component={Landing} />
    //   </div>
    // );
      {/*<div>
        <Header user={{ _id: '5ba77d9e16a92c079273331c' }} />
        <div>
          <Route exact path="/" component={() => { return <Main user={{ _id: '5ba77d9e16a92c079273331c' }} /> }} />
          <Route path="/playgrounds" component={Playgrounds} />
        </div>
      </div>*/}
  }
}

export default App;
